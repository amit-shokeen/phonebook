# Simple Phonebook Application Readme #

By: Amit Shokeen

This README would normally document whatever steps are neccessary to get your application up and running.

### Phinx Migration ###

It creates users and contacts tables.

Installation
------------
#Step1: Install required packages. 

```bash
	composer update
```

#Step2: Run migrations.

```bash
	vendor/bin/phinx migrate
```
#Step3: Seeds a default user to login into the application.

```bash
	vendor/bin/phinx seed:run
```

# Access credentials #

You can use following credentials to access the admin:

Email : admin@example.com
Password: password




