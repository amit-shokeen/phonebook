<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Phonebook Lang - English
*
* Author: Amit Shokeen
* 		 amit@saicosys.com
*
* Location: https://bitbucket.org/amit-shokeen/phonebook
*
* Created:  05.02.2018
*
* Description:  English language file for Phonebook views
*
*/

// Errors
$lang['error_csrf'] = 'This form post did not pass our security checks.';

//Index 
$lang['index_title']= 'Phonebook contact list';
$lang['add_title']	= 'Add contact';
$lang['edit_title']	= 'Edit contact';
$lang['delete_title']= 'Delete contact';
$lang['view_title'] = 'View contact';
// Add, edit form
$lang['name']		= 'Name';
$lang['first_name']	= 'First Name<span class="text-danger">*</span>';
$lang['last_name']  = 'Last Name<span class="text-danger">*</span>';
$lang['email']  	= 'Email<span class="text-danger">*</span>';
$lang['phone']  	= 'Phone Number';
$lang['mobile']  	= 'Mobile Nnumber<span class="text-danger">*</span>';
$lang['address']    = 'Address';
$lang['date']    	= 'Date';
$lang['note'] 		= 'Additional Note';

// Add, edit, delete
$lang['new_contact_added'] = 'New contact has been created successfuly';
$lang['new_contact_updated'] = 'contact has been updated successfuly';
$lang['response_error'] = 'Something went wrong. Please try after some time';
$lang['data_not_found_error'] = 'No data found';
$lang['data_deleted'] = 'Contact successfuly deleted';
$lang['data_not_delete'] = 'Contact could not deleted';

// buttons
$lang['search'] 	= 'Search';
$lang['save'] 		= 'Create contact';
$lang['reset'] 		= 'Reset';
$lang['user_back'] 	= 'Back';
$lang['created'] 	= 'Created';
$lang['modified'] 	= 'Updated';
$lang['edit_contact_back'] 	= 'Back';
$lang['edit_contact_submit_btn'] 	= 'Update Contact';
$lang['add_user'] 	= 'Add Contact';
$lang['add_group'] 	= 'Add Group';