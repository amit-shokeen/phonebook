<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  App Lang - English
*
* Author: Amit Shokeen
* 		 amit@saicosys.com
*
* Description:  English language file for App view
*
*/

// Errors
$lang['error_csrf'] = 'This form post did not pass our security checks.';

// Login / Logout
$lan['login_title']						= 'Login';
$lang['login_successful']               = 'Logged In Successfully';
$lang['login_unsuccessful']             = 'Incorrect email address & password';
$lang['login_unsuccessful_not_active']  = 'Account is inactive';
$lang['login_timeout']                  = 'Temporarily Locked Out.  Try again later.';
$lang['logout_successful']              = 'Logged Out Successfully';

// Login
$lang['login_heading']         = 'Login';
$lang['login_subheading']      = 'Login with your email and password below.';
$lang['login_identity_label']  = 'Email/Username:';
$lang['login_password_label']  = 'Password';
$lang['login_remember_label']  = 'Remember Me';
$lang['login_submit_btn']      = 'Login';
$lang['login_forgot_password'] = 'Forgot your password?';
