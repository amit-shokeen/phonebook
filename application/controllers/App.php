<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App extends CI_Controller {

	public function __construct()
	{
			parent::__construct();
			$this->load->model("App_model", "App");
			$this->lang->load('app');
			$this->load->library('form_validation','session');
	} 
	
	/**
     * Login method 
     * 
     * @return mixed
     *
     * Login page view
     */
	public function login()
	{
		$data['title'] = $this->lang->line('login_heading');
		$this->template->load('auth_template', 'login', $data);
	}
	
	/**
     * Validate account method 
     * 
     * @return true or false
     *
     * Validate user on login
     */
	public function validate_account()
	{
		$data['title'] = $this->lang->line('login_heading');
		if ($this->input->method() === 'post') {
			$this->form_validation->set_rules('email', $this->lang->line('login_identity_label'), 'trim|xss_clean|required|valid_email');
			$this->form_validation->set_rules('password', $this->lang->line('login_password_label'), 'trim|xss_clean|required');
			
			if ($this->form_validation->run() === TRUE)
			{
				$check_user = $this->App->verify_user($this->input->post('email', TRUE),$this->input->post('password', TRUE));
				if($check_user){
					$this->session->set_flashdata('success_message', $this->lang->line('login_successful'));
					redirect('/phonebook');
				}else{
					$this->session->set_flashdata('error_message', $this->lang->line('login_unsuccessful'));
					redirect('/', 'refresh');
				}
			}else{
				$this->template->load('auth_template', 'login');
			}
		}else{
			$this->template->load('auth_template', 'login', $data);
		}
        
	}
	
	/**
     * logout method 
     * 
     * @return true or false
     *
     * Logout user from the active session
     */
	public function logout(){
		$this->session->unset_userdata('sys_admin');
		$this->session->sess_destroy();	
		redirect('/', 'refresh	');
	}
}
