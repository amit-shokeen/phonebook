<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Phonebook Class
 *
 * @author Amit Shokeen
 * @link https://bitbucket.org/amit-shokeen/phonebook
 */
 
class Phonebook extends CI_Controller {
	
    public function __construct()
	{
		parent::__construct();
        $this->load->model('Phonebook_model', 'Phonebook');
		$this->load->model('App_model', 'App');
		$this->lang->load('phonebook');
        $this->config->load('pagination', TRUE);
        if ($this->App->check_user($this->session->userdata('sys_admin')) === FALSE) {
            $this->session->set_flashdata('error_message', 'You must be login to view this page');
            redirect("/", 'refresh');
        }
	}
    
    /**
     * Index method 
     *
     * Phonebook contact list with pagination
     * Default page show 10/per page
     *
     * @return mixed
     */
	public function index()
	{
        $data['title']             = $this->lang->line('index_title');
        if ($this->input->method() === 'get') {
            $data = [
                'name'      => $this->input->get('name', TRUE),
                'phone'     => $this->input->get('phone', TRUE),
                'date_added'=> $this->input->get('date_added', TRUE),
                'note'      => $this->input->get('note', TRUE)
            ];

        }
        // access pagination settings
        $settings = $this->config->item('pagination');
        $settings['total_rows'] = $this->Phonebook->count_all($data);
        $choice                 = $settings['total_rows'] / $settings['per_page'];
        $settings['num_links']  = floor($choice);
       $this->pagination->initialize($settings);
        $page = ($this->uri->segment($settings['uri_segment'])) ? $this->uri->segment($settings['uri_segment']): 0;
        $data['results']    = $this->Phonebook->get_contacts($data, $settings['per_page'], $page);
        $data['pagination'] = $this->pagination->create_links();
        $this->template->load('template', 'phonebook/index', $data);
	}

    /**
     * Add method 
     *
     * Add new phonebook contact
     *
     * @return true|false
     */
	public function add()
	{
        $data['title']           = $this->lang->line('add_title');
		$data['success_message'] = '';
        $data['error_message']   = '';
        
		if ($this->input->method() === 'post') {

			$this->form_validation->set_rules('name', $this->lang->line('name'), 'trim|xss_clean|required|alpha_numeric');
			$this->form_validation->set_rules('phone', $this->lang->line('phone'), 'trim|required|xss_clean|integer|max_length[15]');
			$this->form_validation->set_rules('date', $this->lang->line('date'), 'trim|xss_clean');
			$this->form_validation->set_rules('note', $this->lang->line('note'), 'trim|xss_clean');

			if ($this->form_validation->run() === TRUE) {
				$data = [
					'name'      => $this->input->post('name', TRUE),
					'phone' 	=> $this->input->post('phone', TRUE),
					'date_added'=> $this->input->post('date', TRUE),
					'note' 		=> $this->input->post('note', TRUE),
					'user_id' 	=> $this->session->userdata('sys_admin'),
				];
				$response = $this->Phonebook->create($data);
                if($response){
                    $this->session->set_flashdata('success_message', $this->lang->line('new_contact_added'));
				    redirect('phonebook');
                }else{
                    $this->session->set_flashdata('success_message', $this->lang->line('response_error')); 
                }
			} else {
				$data['error_message'] = validation_errors();
			}
		}

		$this->template->load('template', 'phonebook/add', $data);
	}

    /**
     * Edit method 
     *
     * Edit any phonebook contact
     *
     * @return true|false
     */
    public function edit($id)
    {
        $data['title']           = $this->lang->line('edit_title');
        $data['success_message'] = '';
        $data['error_message']   = '';
        if ($this->input->method() === 'post') {
            $this->form_validation->set_rules('name', $this->lang->line('name'), 'trim|xss_clean|required');
            $this->form_validation->set_rules('phone', $this->lang->line('phone'), 'trim|required|xss_clean|integer|max_length[15]');
            $this->form_validation->set_rules('date', $this->lang->line('date'), 'trim|xss_clean');
            $this->form_validation->set_rules('note', $this->lang->line('note'), 'trim|xss_clean');
            if ($this->form_validation->run() == FALSE) {
                $data['error_message'] = validation_errors();
            } else {
                $data = [
					'name'      => $this->input->post('name', TRUE),
                    'phone'     => $this->input->post('phone', TRUE),
                    'date_added'=> $this->input->post('date', TRUE),
                    'note'      => $this->input->post('note', TRUE),
                    'user_id'   => $this->session->userdata('sys_admin'),
				];
                $response = $this->Phonebook->update($id, $data);
                if($response){
                    $this->session->set_flashdata('success_message', $this->lang->line('new_contact_updated'));
                redirect(site_url('phonebook/index'));
                }else{
                    $this->session->set_flashdata('success_message', $this->lang->line('response_error'));
                }
            }
        }
        if ($query = $this->Phonebook->get($id)) {
            $data['result'] = $query->row();
        } else {
            $this->session->set_flashdata('error_message', $this->lang->line('data_not_found_error'));
            redirect(site_url('phonebook/index'));
        }
        $this->template->load('template', 'phonebook/edit', $data);
    }
    
    /**
     * View method 
     *
     * View any phonebook contact
     *
     * @param $id
     *
     * @return mixed
     */
    public function view($id)
    {
        $data['title'] = $this->lang->line('view_title');
        if ($query = $this->Phonebook->get($id)) {
            $data['result'] = $query->row();
        } else {
            $this->session->set_flashdata('error_message', $this->lang->line('data_not_found_error'));
            redirect(site_url('phonebook/index'));
        }
        $this->template->load('template', 'phonebook/view', $data);
    }

    /**
     * Delete method 
     *
     * Delete any phonebook contact
     *
     * @param $id
     *
     * @return true|false
     */
    public function delete($id)
    {
        $data['title'] = $this->lang->line('delete_title');
        if ($this->Phonebook->delete($id)) {
            $this->session->set_flashdata('success_message', $this->lang->line('data_deleted'));
        } else {
            $this->session->set_flashdata('error_message', $this->lang->line('data_not_delete'));
        }
        redirect(site_url('phonebook/index'));
    }
}
