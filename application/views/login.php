<div class="panel-heading text-center">
    <h1><?php echo lang('login_heading');?></h1>
    <p><?php echo lang('login_subheading');?></p>
</div>
<div class="panel-body">
    <?php echo form_open("app/validate_account", array('method' => 'post'));?>
        <fieldset>
            <div class="form-group">
                <?php echo form_input(array('name' => 'email', 'type' => 'email', 'class' => 'form-control', 'placeholder' => 'Email','value' => set_value('email')));?>
                <?php echo (form_error('email')) ? '<small class="text-help text-danger">'.form_error('email').'</small>' : ''; ?>
            </div>
            <div class="form-group">
                <?php echo form_input(array('name' => 'password', 'type' => 'password', 'class' => 'form-control', 'placeholder' => '*******'));?>
                <?php echo (form_error('password')) ? '<small class="text-help text-danger">'.form_error('password').'</small>' : ''; ?>
            </div>
            <?php echo form_submit(array('name' => 'submit', 'type' => 'submit', 'class' => 'btn btn-lg btn-success btn-block', 'value' => lang('login_submit_btn')));?>
        </fieldset>
    <?php echo form_close();?>
</div>