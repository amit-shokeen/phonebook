<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
    <div class="col-md-6">
        <h3><?php echo (!empty($title)) ? $title : 'Phonebook contact list' ?></h3>
    </div>
    <div class="col-md-6">
        <div class="text-right">
            <h3><a href="<?php echo base_url('phonebook/add');?>" class="btn btn-primary">Add Contact</a></h3>
        </div>
    </div>
</div>

<div class="row">
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>Contact list</h4>
        </div>
        <div class="panel-body">
        <?php echo form_open("phonebook", array('method' => 'get', 'class' => 'search-filter'));?>
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <?php echo form_input(array('name' => 'name', 'type' => 'text', 'placeholder' => 'e.g. name', 'class' => 'form-control', 'value' => (!empty($name)) ? $name : ''));?>
                        <?php echo (form_error('name')) ? '<small class="text-help text-danger">'.form_error('name').'</small>' : ''; ?>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <?php echo form_input(array('name' => 'phone', 'type' => 'text', 'placeholder' => 'e.g. phone no.', 'class' => 'form-control', 'value' => (!empty($phone)) ? $phone : ''));?>
                        <?php echo (form_error('phone')) ? '<small class="text-help text-danger">'.form_error('phone').'</small>' : ''; ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?php echo form_input(array('name' => 'date_added', 'type' => 'date', 'class' => 'form-control', 'value' => (!empty($date_added)) ? $date_added : ''));?>
                        <?php echo (form_error('date_added')) ? '<small class="text-help text-danger">'.form_error('date_added').'</small>' : ''; ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?php echo form_input(array('name' => 'note', 'type' => 'text', 'placeholder' => 'e.g.  addtional note', 'class' => 'form-control', 'value' => (!empty($note)) ? $note : ''));?>
                        <?php echo (form_error('note')) ? '<small class="text-help text-danger">'.form_error('note').'</small>' : ''; ?>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                       <button type="submit" class="btn btn-info btn-block"><?php echo $this->lang->line('search');?></button>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th><?php echo $this->lang->line('name');?></th>
                    <th><?php echo $this->lang->line('phone');?></th>
                    <th><?php echo $this->lang->line('date');?></th>
                    <th><?php echo $this->lang->line('note');?></th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>

            <tbody>
                <?php 
                    if(!empty($results)){ 
                        foreach ($results as $result){ ?>
                <tr>
                    <td><a href="<?= site_url('phonebook/edit/' . $result->id) ?>"><?= $result->name; ?></a></td>
                    <td><?php echo $result->phone; ?></td>
                    <td><?php echo ($result->date_added != '0000-00-00 00:00:00') ? date('d M Y', strtotime($result->date_added)) : '0000-00-00 00:00:00';; ?></td>
                    <td><?php echo $result->note; ?></td>
                    <td class="text-center">
                        <a href="<?= site_url('phonebook/edit/' . $result->id) ?>" class="btn btn-warning btn-xs">Edit</a>
                        <a href="<?= site_url('phonebook/view/' . $result->id) ?>" class="btn btn-success btn-xs">View</a>
                        <a href="<?= site_url('phonebook/delete/' . $result->id) ?>" class="btn btn-danger btn-xs confirm">Delete</a>
                    </td>
                <?php } }else{ ?>
                <td colspan="6" class="text-center"> No result found!</td>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="clearfix"></div>
    <?php echo $pagination;?>
</div>
