<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
    <div class="col-md-12">
        <h3 class="page-header"><?php echo (!empty($title)) ? $title : 'No title' ?></h3>
    </div>
</div>
<?php echo form_open(uri_string());?>
<div class="row">
	<div class="col-md-6">
		<fieldset>
		  	<div class="form-group">
				<?php echo form_label($this->lang->line('name'),'name'); ?>
			    <?php echo form_input(array('name' => 'name', 'type' => 'text', 'class' => 'form-control', 'value' => (!empty($result->name)) ? $result->name : set_value('name')));?>
			    <?php echo (form_error('name')) ? '<small class="text-help text-danger">'.form_error('name').'</small>' : ''; ?>
			</div>
			<div class="form-group">
			    <label><?php echo $this->lang->line('phone');?></label>
			    <?php echo form_input(array('name' => 'phone', 'type' => 'text', 'class' => 'form-control','value' => (!empty($result->phone)) ? $result->phone : set_value('phone')));?>
			    <?php echo (form_error('phone')) ? '<small class="text-help text-danger">'.form_error('phone').'</small>' : ''; ?>
			</div>
			<div class="form-group">
			    <?php echo form_label($this->lang->line('date'),'date'); ?>
			    <?php echo form_input(array('name' => 'date', 'type' => 'date', 'class' => 'form-control','value' => (!empty($result->date_added)) ? $result->date_added : set_value('date')));?>
			    <?php echo (form_error('date')) ? '<small class="text-help text-danger">'.form_error('date').'</small>' : ''; ?>
			</div>
		</fieldset>
	</div>
	
	<div class="col-md-6">
		<fieldset>
			<div class="form-group">
			    <?php echo form_label($this->lang->line('note'),'note'); ?>
			    <?php echo form_textarea(array('name' => 'note', 'rows' => '8', 'class' => 'form-control','value' => (!empty($result->note)) ? $result->note : set_value('note')));?>
			    <?php echo (form_error('note')) ? '<small class="text-help text-danger">'.form_error('note').'</small>' : ''; ?>
			</div>
		</fieldset>
	</div>
	
  	<div class="col-md-12">
    <a href="<?php echo base_url('phonebook');?>" class="btn btn-success"><?php echo $this->lang->line('edit_contact_back');?></a>
    <button type="submit" class="btn btn-primary"><?php echo lang('edit_contact_submit_btn');?></button>
  </div>
</div>
<?php echo form_close();?>
