<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
    <div class="col-md-12">
        <h3 class="page-header"><?php echo (!empty($title)) ? $title : 'No title' ?></h3>
    </div>
</div>
<div class="row">
	<div class="col-md-6"><p><strong><?php echo $this->lang->line('name'); ?></strong></p></div>
	<div class="col-md-6"><p><?php echo $result->name; ?></p></div>
</div>
<div class="row">
	<div class="col-md-6"><p><strong><?php echo $this->lang->line('phone'); ?></strong></p></div>
	<div class="col-md-6"><p><?php echo $result->phone; ?></p></div>
</div>
<div class="row">
	<div class="col-md-6"><p><strong><?php echo $this->lang->line('date'); ?></strong></p></div>
	<div class="col-md-6"><p><?php echo ($result->date_added != '0000-00-00 00:00:00') ? date('d M Y', strtotime($result->date_added)) : '0000-00-00 00:00:00'; ?></p></div>
</div>
<div class="row">
	<div class="col-md-6"><p><strong><?php echo $this->lang->line('note'); ?></strong></p></div>
	<div class="col-md-6"><p><?php echo $result->note; ?></p></div>
</div>
<div class="row">
	<div class="col-md-6"><p><strong><?php echo $this->lang->line('created'); ?></strong></p></div>
	<div class="col-md-6"><p><?php echo date('d M Y', strtotime($result->created)); ?></p></div>
</div>
<div class="row">
	<div class="col-md-6"><p><strong><?php echo $this->lang->line('modified'); ?></strong></p></div>
	<div class="col-md-6"><p><?php echo ($result->modified != '0000-00-00 00:00:00') ? date('d M Y', strtotime($result->modified)) : '0000-00-00 00:00:00'; ?></p></div>
</div>
