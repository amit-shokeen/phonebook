<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
    <head lang="en">
        <meta charset="utf-8" />
        <title><?php echo (!empty($title)) ? $title : 'Phonebook'; ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('/dist/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
         <!-- MetisMenu CSS -->
        <link href="<?php echo base_url('/dist/metisMenu/metisMenu.min.css'); ?>" rel="stylesheet">
        <!-- Font Awesome Fonts -->
        <link href="<?php echo base_url('/dist/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">
        <!-- Custom css -->
        <link href="<?php echo base_url('/dist/css/style.css'); ?>" rel="stylesheet" type="text/css" />

    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <?php echo ($this->session->flashdata('error_message')) ? '<div class="alert alert-danger alert-dismissible" role="alert" style="margin-bottom:0;"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Error! </strong>'.$this->session->flashdata('error_message').'</div>' : ''; ?>
                        <?php echo ($this->session->flashdata('success_message')) ? '<div class="alert alert-success alert-dismissible" role="alert" style="margin-bottom:0;"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Success! </strong>'.$this->session->flashdata('success_message').'</div>' : ''; ?> 
                        <?php echo $contents ?> 
                    </div>
                </div>
            </div>
        </div>      
        <!-- jQuery -->
        <script src="<?php echo base_url('/dist/jquery/jquery.min.js'); ?>"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="<?php echo base_url('/dist/bootstrap/js/bootstrap.min.js'); ?>"></script>
        <!-- Metis Menu Plugin JavaScript -->
        <script src="<?php echo base_url('/dist/metisMenu/metisMenu.min.js'); ?>"></script>
        <!-- Custom Theme JavaScript -->
        <script src="<?php echo base_url('/dist/js/custom.js'); ?>"></script>
    </body>
</html>
