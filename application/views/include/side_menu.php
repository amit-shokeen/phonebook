<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="navbar-default sidebar" role="navigation">
	<div class="sidebar-nav navbar-collapse">
		<ul class="nav" id="side-menu">
			<li>
				<a href="<?php echo base_url('phonebook');?>"><i class="fa fa-phone fa-fw"></i> Phonebook</a>
			</li>
		</ul>
	</div>
</div>
