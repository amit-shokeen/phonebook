<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
    <head lang="en">
        <meta charset="utf-8" />
        <title><?php echo (!empty($title)) ? $title : 'Phonebook'; ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('/dist/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
         <!-- MetisMenu CSS -->
        <link href="<?php echo base_url('/dist/metisMenu/metisMenu.min.css'); ?>" rel="stylesheet">
        <!-- Font Awesome css -->
        <link href="<?php echo base_url('/dist/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('/dist/css/style.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- Custom Fonts -->
    </head>
    <body class="">
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <!-- navbar-top-links -->
                <?php $this->load->view('include/top_menu.php'); ?>
                <!-- /.navbar-top-links -->
                 <!-- navbar-static-side -->
                <?php $this->load->view('include/side_menu.php'); ?>
                 <!-- /.navbar-static-side -->
            </nav>
            <div id="page-wrapper">
                <!-- panel-heading -->
                <div class="panel-body">
                    <?php echo ($this->session->flashdata('error_message')) ? '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Error! </strong>'.$this->session->flashdata('error_message').'</div>' : ''; ?>
                    <?php echo ($this->session->flashdata('success_message')) ? '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Success! </strong>'.$this->session->flashdata('success_message').'</div>' : ''; ?> 
                    
                    <?php echo $contents ?>	
                </div>
                <!-- /.panel-heading -->  
            </div>         
            <!-- jQuery -->
            <script src="<?php echo base_url('/dist/jquery/jquery.min.js'); ?>"></script>
            <!-- Bootstrap Core JavaScript -->
            <script src="<?php echo base_url('/dist/bootstrap/js/bootstrap.min.js'); ?>"></script>
            <!-- Metis Menu Plugin JavaScript -->
            <script src="<?php echo base_url('/dist/metisMenu/metisMenu.min.js'); ?>"></script>
            <!-- Custom Theme JavaScript -->
            <script src="<?php echo base_url('/dist/js/custom.js'); ?>"></script>
            <!-- Page-Level Demo Scripts - Tables - Use for reference -->
            <script>
            $(document).ready(function() {
                $('#dataTables-example').DataTable({
                    responsive: true
                });
            });
            </script>
    </body>
</html>
