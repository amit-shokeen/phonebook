<?php
/**
 * Name:    App Model
 * Author:  Amit Shokeen <amit@saicosys.com>
 *       
 * @amit-shokeen
 *
 * Created:  06.02.2018
 *
 * Requirements: PHP5 or above
 *
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class App Model
 */
class App_model extends CI_Model
{
	/**
     * verify_user method 
     * 
     * @param $email, $password
     *
     * Verify user
     */
    public function verify_user($email,$password){
		$response = FALSE;
		$this->db->where('email',$email);
		$this->db->where('active',1);
		$query = $this->db->get('users');
		if($query->num_rows() == 1){
			$data = $query->row();
			if ($this->bcrypt->verify($password,$data->password))
			{
				$this->session->set_userdata("sys_admin", $data->id);
				$response = TRUE;
			} else{
				$response = FALSE;
			}
		}
		return $response;
	}
	
	/**
     * checkUser method 
     * 
     * @param $id
     *
     * Check user session
     */
	public function check_user($id){
		$response = FALSE;
        if (!empty($id)) {
			$this->db->where('id',$id);
			$this->db->where('active',1);
			$query = $this->db->get('users');
			if($query->num_rows() == 1){
				$response = TRUE;
			}
		}
		return $response;
	}
}
