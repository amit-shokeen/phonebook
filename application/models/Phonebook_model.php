<?php
/**
 * Name:    Phonebook Model
 * Author:  Amit Shokeen <amit@saicosys.com>
 *       
 * @amit-shokeen
 *
 * Created:  06.02.2018
 *
 * Requirements: PHP5 or above
 *
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Phonebook_model Model
 */
class Phonebook_model extends CI_Model
{
    /**
     * Create method 
     * 
     * @param $data
     *
     * Create new contact
     */
	public function create($data)
	{
		$this->db->insert('contacts', $data);
		return $this->db->insert_id();
	}

    /**
     * Update method 
     * 
     * @param $id|$data
     *
     * Update contact
     */
    public function update($id, $data)
    {
        $this->db->where('id', $id);
        return $this->db->update('contacts', $data);
    }

    /**
     * Create method 
     * 
     * @param $id
     *
     * Delete any contact on basis of its Id
     * @return true|false
     */
    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('contacts');
        return (bool) $this->db->affected_rows();
    }

    /**
     * Get method 
     * 
     * @param $id
     *
     * Get all contact data on basis of $id
     *
     * @return object $id
     */
    public function get($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('contacts');
        if ($query->num_rows() > 0) {
            return $query;
        }
        return false;
    }

    /**
     * Get Contact method 
     * 
     * @param $limit
     * @param $start
     *
     * Get contact list on basis of custom pagination
     *
     * @return object list 
     */
    public function get_contacts($search = null, $limit, $start)
	{
        $response  = false;
        if(!empty($search['name'])){
            $this->db->like('name', $search['name'], 'after');
        } else if(!empty($search['phone'])){
            $this->db->like('phone', $search['phone'], 'after');
        } else if(!empty($search['note'])){
            $this->db->like('note', $search['note'], 'after');
        } else if(!empty($search['date_added'])){
            $this->db->or_where('date_added =', $search['date_added']);
        }
        $this->db->limit($limit, $start);
        $query = $this->db->get('contacts');
        if ($query->num_rows() > 0) {
            $response = $query->result();
        }
        return $response;
	}

    /**
     * Count all method 
     * 
     * @param $search
     * Count all contacts
     *
     * @return init
     */
	public function count_all($search = NULL)
	{
        if(!empty($search['name'])){
            $this->db->like('name', $search['name'], 'after');
        } else if(!empty($search['phone'])){
            $this->db->like('phone', $search['phone'], 'after');
        } else if(!empty($search['note'])){
            $this->db->like('note', $search['note'], 'after');
        } else if(!empty($search['date_added'])){
            $this->db->or_where('date_added =', $search['date_added']);
        }
        $data = count($this->db->get('contacts')->result());
       
        return $data;       
	}

    /**
     * truncate method 
     * 
     * truncate contacts
     */
	public function truncate()
	{
        $this->db->truncate('contacts');
	}
}
