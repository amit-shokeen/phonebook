<?php


use Phinx\Migration\AbstractMigration;

class Contacts extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {	
		$contacts = $this->table('contacts', ['unsigned' => true,]);
        $contacts->addColumn('name', 'string', ['limit' => 45])
              ->addColumn('phone', 'string', ['limit' => 100])
              ->addColumn('password', 'string', ['limit' => 80])
              ->addColumn('date_added', 'datetime', ['limit' => 40])
              ->addColumn('note', 'text', ['limit' => 40])
              ->addColumn('user_id', 'string', ['limit' => 40])
              ->addColumn('modified', 'timestamp', ['default' => '0000-00-00 00:00:00'])
              ->addColumn('created', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
              ->save();
    }
	
	public function down() {
        $this->dropTable('contacts');
    }
}
