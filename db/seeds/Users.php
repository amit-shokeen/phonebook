<?php


use Phinx\Seed\AbstractSeed;

class Users extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
		// inserting only one row
        $data = [
            'email'      => 'admin@example.com',
            'password'   => '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36',
            'active'  	 => '1',
			'first_name' => 'Admin',
			'last_name'  => 'example',
			'last_login' => date('Y-m-d'),
			'created_on' => date('Y-m-d'),
        ];

        $table = $this->table('users');
        $table->insert($data);
        $table->saveData();
    }
}
