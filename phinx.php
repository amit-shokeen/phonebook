<?php
	require_once 'application/config/app-config.php';
	return [
		'paths' => [
			'migrations' => '%%PHINX_CONFIG_DIR%%/db/migrations',
			'seeds'		=> '%%PHINX_CONFIG_DIR%%/db/seeds'
		],
		'environments' => [
			'development' => [
				'adapter' => 'mysql',
				'host' 	=> APP_DB_HOSTNAME,
				'name'	=> APP_DB_NAME,
				'user'	=> APP_DB_USERNAME,
				'pass'	=> APP_DB_PASSWORD,
				'port' 	=> 3306,
				'charset' => 'utf8'
			]
		],
		'version_order' =>  'creation'
	];